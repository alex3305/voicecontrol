# Voice control for Java

Voice control module based on Sphinx4. This is a partial work in progress and dependend
on the Voxforge-nl artifact.

## Dependencies

The `org.voxforge:voxforge-nl:0.1` package must be present on the classpath to successfully run this project. It
is recommended that this is added through Maven, although disabled for testing purposes.