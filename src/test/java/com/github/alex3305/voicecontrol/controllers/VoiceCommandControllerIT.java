package com.github.alex3305.voicecontrol.controllers;

import org.junit.Test;

import java.nio.file.Path;
import java.nio.file.Paths;

public class VoiceCommandControllerIT {

    Path p = Paths.get(".", "com/github/alex3305/voicecontrol");

    @Test
    public void test() throws InterruptedException {
        new VoiceCommandController(p).run();
    }

}