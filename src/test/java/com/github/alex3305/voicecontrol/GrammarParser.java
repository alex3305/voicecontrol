package com.github.alex3305.voicecontrol;

import edu.cmu.sphinx.jsgf.JSGFGrammar;
import edu.cmu.sphinx.jsgf.parser.JSGFParser;
import edu.cmu.sphinx.linguist.acoustic.UnitManager;
import edu.cmu.sphinx.linguist.dictionary.Dictionary;
import edu.cmu.sphinx.linguist.dictionary.TextDictionary;

import java.io.IOException;
import java.net.URL;
import java.nio.file.Path;
import java.util.ArrayList;

public class GrammarParser {

    private static Dictionary DICTIONARY;

    private static JSGFGrammar GRAMMAR;

    public static Dictionary createDictionary(URL path, URL filler) throws IOException {
        if (DICTIONARY == null) {
            Dictionary dictionary = new TextDictionary(path, filler, new ArrayList<>(), null, new UnitManager());
            dictionary.allocate();
            DICTIONARY = dictionary;
        }

        return DICTIONARY;
    }

    public static JSGFGrammar createGrammar(URL basePath, String fileName, Dictionary dictionary) throws IOException {
        if (GRAMMAR == null) {
            JSGFGrammar grammar = new JSGFGrammar(basePath, fileName, false, true, false, false, dictionary);
            grammar.allocate();
            GRAMMAR = grammar;
        }

        return GRAMMAR;
    }

    public void parse(URL basePath, String fileName) throws IOException {
        Dictionary d = createDictionary(
                this.getClass().getResource("/com/github/alex3305/voicecontrol/dict/nl_NL.dict"),
                this.getClass().getResource("/com/github/alex3305/voicecontrol/dict/nl_NL_filler.dict"));
        JSGFGrammar grammar = createGrammar(basePath, fileName, d);
        grammar.getRuleGrammar().getJSGFTags("zet balkonverlichting aan");
//        JSGFParser p = new JSGFParser(new In);
    }

    public void parse(Path p) {

    }

}
