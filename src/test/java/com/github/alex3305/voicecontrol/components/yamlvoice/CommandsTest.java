package com.github.alex3305.voicecontrol.components.yamlvoice;

import org.junit.Test;

import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Created by alex on 9-4-2016.
 */
public class CommandsTest {

    @Test
    public void testCreate() throws Exception {
        Path p = Paths.get(this.getClass().getClassLoader().getResource("com/github/alex3305/voicecontrol/grammar/commands.yaml").toURI());
        Commands.create(p);
    }
}