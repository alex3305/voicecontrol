package com.github.alex3305.voicecontrol.components;

import org.junit.Ignore;
import org.junit.Test;

import java.nio.file.Path;
import java.nio.file.Paths;

import static org.junit.Assert.*;

/**
 * Created by alex on 9-4-2016.
 */
public class SoundPlayerIT {

    @Test
    public void testPlay() throws Exception {
        Path p = Paths.get(this.getClass().getClassLoader().getResource("com/github/alex3305/voicecontrol/sounds/gets-in-the-way.wav").toURI());
        SoundPlayer.play(p);
    }
}