package com.github.alex3305.voicecontrol.components;

import org.junit.Test;

public class KeywordSpotterIT {

    private static final String KEYWORD_PATH = "/com/github/alex3305/voicecontrol/grammar/";

    private static final String KEYWORD_NAME = "hotword";

    @Test
    public void testSpot() throws Exception {
        new KeywordSpotter(System.out::println, KEYWORD_PATH, KEYWORD_NAME, "lazy homey").spot();
    }
}