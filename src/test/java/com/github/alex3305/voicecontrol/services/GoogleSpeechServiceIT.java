package com.github.alex3305.voicecontrol.services;

import com.github.alex3305.voicecontrol.components.SoundRecorder;
import com.github.alex3305.voicecontrol.components.yamlvoice.Commands;
import org.junit.Assert;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;

/**
 * Created by alex on 8-4-2016.
 */
public class GoogleSpeechServiceIT {

    @Test
    public void testLiveTranscribe() throws Exception {
        List<String> response = GoogleSpeechService.transcribe(SoundRecorder.record(5000), GoogleSpeechService.Languages.NL);
        System.out.println(response);
    }

    @Test
    public void testFileTranscribe() throws IOException {
        List<String> response = GoogleSpeechService.transcribe(new FileInputStream("test.flac"), GoogleSpeechService.Languages.NL);
        System.out.println(response);
    }

    @Test
    public void testFileTranscribeAndMatch() throws IOException, URISyntaxException {
        Path p = Paths.get(this.getClass().getClassLoader().getResource("com/github/alex3305/voicecontrol/grammar/commands.yaml").toURI());
        Commands c = Commands.create(p);

        List<String> response = GoogleSpeechService.transcribe(new FileInputStream("test.flac"), GoogleSpeechService.Languages.NL);
        Optional<String> command = Optional.empty();

        for (String utterance : response) {
            command = c.match(utterance);
            if (command.isPresent()) {
                break;
            }
        }

        Assert.assertNotNull(command);
    }
}