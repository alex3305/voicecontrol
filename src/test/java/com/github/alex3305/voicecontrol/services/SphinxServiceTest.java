package com.github.alex3305.voicecontrol.services;

import edu.cmu.sphinx.api.Configuration;
import edu.cmu.sphinx.api.LiveSpeechRecognizer;
import org.junit.Assert;
import org.junit.Test;

public class SphinxServiceTest {

    private static final String KEYWORD_PATH = "/com/github/alex3305/voicecontrol/grammar/";

    private static final String KEYWORD_NAME = "hotword";

    @Test
    public void testSetupSphinx() throws Exception {
        Configuration configuration = new SphinxService().setupSphinx(KEYWORD_PATH, KEYWORD_NAME);
        Assert.assertNotNull(configuration);
    }
}