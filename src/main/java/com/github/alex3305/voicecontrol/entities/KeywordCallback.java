package com.github.alex3305.voicecontrol.entities;

@FunctionalInterface
public interface KeywordCallback {

    void trigger(String hypothesis);

}
