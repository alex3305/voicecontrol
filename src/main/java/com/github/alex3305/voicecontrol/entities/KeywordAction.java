package com.github.alex3305.voicecontrol.entities;

@FunctionalInterface
public interface KeywordAction {

    void trigger() throws Exception;

}
