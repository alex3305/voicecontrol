package com.github.alex3305.voicecontrol.components;

import com.github.alex3305.voicecontrol.entities.KeywordCallback;
import com.github.alex3305.voicecontrol.services.SphinxService;
import edu.cmu.sphinx.api.LiveSpeechRecognizer;
import edu.cmu.sphinx.api.SpeechResult;
import edu.cmu.sphinx.result.WordResult;

import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.TargetDataLine;
import java.io.IOException;
import java.time.LocalDateTime;

public class KeywordSpotter {

    private static final int MINIMUM_SCORE = 100;

    private final KeywordCallback callback;

    private final String hotword;

    private LiveSpeechRecognizer liveSpeechRecognizer;

    private TargetDataLine targetDataLine;

    public KeywordSpotter(KeywordCallback callback, String keywordPath, String fileName, String hotword) throws IOException {
        this.liveSpeechRecognizer = new SphinxService().createRecognizer(keywordPath, fileName);
        this.callback = callback;
        this.hotword = hotword;
        this.acquireTargetDataLine();
    }

    private void acquireTargetDataLine() {
        try {
            this.targetDataLine = SphinxService.acquireSphinxTargetDataLine(this.liveSpeechRecognizer);
        } catch (IllegalAccessException | NoSuchFieldException e) {
            e.printStackTrace(); // fixme.
        }
    }

    public void spot() {
        liveSpeechRecognizer.startRecognition(true);
        SpeechResult result;

        while ((result = liveSpeechRecognizer.getResult()) != null) {
//            final long score = calculateScore(result);
//            System.out.println("Received " + result.getHypothesis());
            if (result.getHypothesis().endsWith(this.hotword)) {
                System.out.println("[" + LocalDateTime.now().toString() + "] Received "
                        + result.getHypothesis());

                liveSpeechRecognizer.stopRecognition();
                this.targetDataLine.close();

                this.callback.trigger(result.getHypothesis());

                try {
                    this.targetDataLine.open(SphinxService.SPHINX_AUDIO_FORMAT);
                } catch (LineUnavailableException e) {
                    e.printStackTrace(); // fixme..
                }
                liveSpeechRecognizer.startRecognition(false);
            }
        }

        liveSpeechRecognizer.stopRecognition();
    }

    private long calculateScore(SpeechResult result) {
        return Math.round((result.getWords().stream().mapToDouble(WordResult::getScore).sum() / 100000));
    }

}
