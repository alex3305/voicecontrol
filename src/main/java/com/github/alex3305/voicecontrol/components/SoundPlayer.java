package com.github.alex3305.voicecontrol.components;

import javax.sound.sampled.*;
import java.io.IOException;
import java.nio.file.Path;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class SoundPlayer {

    public static void play(Path path) throws IOException, UnsupportedAudioFileException, LineUnavailableException, InterruptedException {
        CountDownLatch latch = new CountDownLatch(1);
        AudioInputStream inputStream = AudioSystem.getAudioInputStream(path.toAbsolutePath().toFile());
        Clip clip = AudioSystem.getClip();

        clip.open(inputStream);
        clip.start();

        new Thread(() -> {
            try {
                Thread.sleep(timeToWait(inputStream));
            } catch (InterruptedException | IOException e) {
                e.printStackTrace(); // fixme.
            }
            latch.countDown();
        }).start();

        latch.await(1, TimeUnit.MINUTES);
    }

    private static long timeToWait(AudioInputStream inputStream) throws IOException {
        if (inputStream == null) {
            throw new IOException("Inputstream is not available");
        }

        return Math.round(1000 * inputStream.getFrameLength() / inputStream.getFormat().getFrameRate());
    }

}
