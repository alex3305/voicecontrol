package com.github.alex3305.voicecontrol.components;

import net.sourceforge.javaflacencoder.FLACFileWriter;

import javax.sound.sampled.*;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class SoundRecorder {

    private static AudioFormat audioFormat = new AudioFormat(44100F, 16, 1, true, false);

    private static AudioFileFormat.Type fileType = FLACFileWriter.FLAC;

    private TargetDataLine line;

    public static InputStream record(final long millis) throws IllegalArgumentException, LineUnavailableException, IOException {
        if (millis > 10000) {
            throw new IllegalArgumentException("Maximum record time is 10 seconds");
        }

        final SoundRecorder recorder = new SoundRecorder();

        new Thread(() -> {
            try {
                Thread.sleep(millis);
            } catch (InterruptedException e) {
                e.printStackTrace(); // fixme.
            }

            recorder.stop();
        }).start();

        ByteArrayOutputStream os = recorder.start();
        return new ByteArrayInputStream(os.toByteArray());
    }

    private ByteArrayOutputStream start() throws LineUnavailableException, IOException {
        DataLine.Info info = new DataLine.Info(TargetDataLine.class, audioFormat);
        line = (TargetDataLine) AudioSystem.getLine(info);
        line.open(audioFormat);
        line.start();

        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        AudioInputStream inputStream = new AudioInputStream(line);
        AudioSystem.write(inputStream, fileType, outputStream);
        return outputStream;
    }

    private void stop() {
        line.stop();
        line.close();
    }

}
