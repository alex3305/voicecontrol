package com.github.alex3305.voicecontrol.components.yamlvoice;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;

import java.io.IOException;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Commands {

    private static ObjectMapper MAPPER = new ObjectMapper(new YAMLFactory());

    private Set<VoiceCommand> commands;

    @JsonProperty("commands")
    private Map<String, Set<String>> rawCommands;

    public static Commands create(Path p) throws IOException {
        return MAPPER.readValue(p.toFile(), Commands.class).createCommands();
    }

    public Optional<String> match(String utterance) {
        return commands.stream().filter(v -> v.match(utterance)).map(VoiceCommand::getName).findAny();
    }

    private Commands createCommands() {
        this.commands = this.rawCommands.entrySet().stream()
                .flatMap(e -> this.toVoiceCommand(e.getKey(), e.getValue()))
                .collect(Collectors.toSet());
        return this;
    }

    private Stream<VoiceCommand> toVoiceCommand(String name, Set<String> keywords) {
        return Stream.of(new VoiceCommand(name, createKeywordSentences(keywords)));
    }

    private Set<String> createKeywordSentences(Set<String> keywords) {
        LinkedList<List<String>> allKeywords = keywords.stream()
                .map(k -> {
                    if (k.contains("|")) {
                        return Arrays.stream(k.split("\\|")).map(String::trim).collect(Collectors.toList());
                    } else {
                        List<String> s = new ArrayList<>();
                        s.add(k.trim());
                        return s;
                    }
                }).collect(Collectors.toCollection(LinkedList::new));

        Set<String> combinations = new TreeSet<>();
        Set<String> newCombinations;

        for (String s : allKeywords.removeFirst()) {
            combinations.add(s);
        }

        while (!allKeywords.isEmpty()) {
            List<String> next = allKeywords.removeFirst();
            newCombinations = new TreeSet<>();

            for (String s1 : combinations) {
                for (String s2 : next) {
                    newCombinations.add(s1 + " " + s2);
                }
            }

            combinations = newCombinations;
        }

        return combinations;
    }

}
