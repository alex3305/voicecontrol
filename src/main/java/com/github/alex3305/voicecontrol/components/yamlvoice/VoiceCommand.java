package com.github.alex3305.voicecontrol.components.yamlvoice;

import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class VoiceCommand {

    private final String name;

    private final Set<String> keywords;

    public VoiceCommand(String name, Set<String> keywords) {
        this.name = name;
        this.keywords = keywords;
    }

    public String getName() {
        return name;
    }

    public Set<String> getKeywords() {
        return keywords;
    }

    public boolean match(String utterance) {
        if (utterance == null || utterance.isEmpty() || !utterance.contains(" ")) {
            return false;
        }

        List<String> uWords = Arrays.stream(utterance.split(" "))
                .map(String::trim)
                .map(String::toLowerCase)
                .collect(Collectors.toList());

        for (String keywords : this.getKeywords()) {
            String[] kWords = keywords.split(" ");
            int i = kWords.length;

            for (String k : kWords) {
                if (uWords.contains(k.trim().toLowerCase())) {
                    i--;
                }
            }

            if (i == 0) {
                return true;
            }
        }

        return false;
    }
}
