package com.github.alex3305.voicecontrol.controllers;

import com.github.alex3305.voicecontrol.entities.KeywordAction;

import java.util.HashMap;
import java.util.Map;

public class ExecutionController {

    private Map<String, KeywordAction> actions = new HashMap<>();

    public void register(String name, KeywordAction action) {
        this.actions.put(name, action);
    }

    public void execute(String name) throws Exception {
        actions.get(name).trigger();
    }

    public boolean contains(String name) {
        return actions.containsKey(name);
    }

}
