package com.github.alex3305.voicecontrol.controllers;

import com.github.alex3305.voicecontrol.components.KeywordSpotter;
import com.github.alex3305.voicecontrol.components.SoundPlayer;
import com.github.alex3305.voicecontrol.components.SoundRecorder;
import com.github.alex3305.voicecontrol.components.yamlvoice.Commands;
import com.github.alex3305.voicecontrol.entities.KeywordCallback;
import com.github.alex3305.voicecontrol.services.GoogleSpeechService;
import com.github.alex3305.voicecontrol.services.KlikAanKlikUitService;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.Logger;

public class VoiceCommandController implements Runnable {

    private static final String KEYWORD_PATH = "/com/github/alex3305/voicecontrol/grammar/";

    private static final String KEYWORD_NAME = "hotword-nl";

    private static final Logger LOGGER = Logger.getLogger(VoiceCommandController.class.getName());

    // User configurable form here

    private static final String HOTWORD = "roze olifant";

    private static final String COMMANDS_PATH = "/resources/grammar/commands.yaml";

    private static final String SOUND_EXECUTED = "/resources/sounds/hey-champ.wav";

    private static final String SOUND_FAILED = "/resources/sounds/gets-in-the-way.wav";

    private static final String SOUND_KEYWORD = "/resources/sounds/served.wav";

    private static final long TIME_TO_RECORD = 3500;

    private final Path basePath;

    private ExecutionController executionController;

    private KeywordSpotter keywordSpotter;

    private Commands commands;

    public VoiceCommandController(Path p) {
        this.basePath = p.toAbsolutePath();
    }

    @Override
    public void run() {
        try {
            this.registerFunctions();
            this.keywordSpotter = new KeywordSpotter(this.recognized(), KEYWORD_PATH, KEYWORD_NAME, HOTWORD);
            this.commands = Commands.create(resolveFromCurrentPath(COMMANDS_PATH));

            // To turn off Sphinx's aggressive logging for now...
            LogManager.getLogManager().reset();
            this.keywordSpotter.spot();
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace(); // fixme
        }
    }

    private KeywordCallback recognized() {
        return hypothesis -> {
            try {
                SoundPlayer.play(resolveFromCurrentPath(SOUND_KEYWORD));

                List<String> utterances = GoogleSpeechService.transcribe(
                        SoundRecorder.record(TIME_TO_RECORD), GoogleSpeechService.Languages.NL);
                for (String s : utterances) {
                    Optional<String> name = this.commands.match(s);
                    if (name.isPresent() && this.executionController.contains(name.get())) {
                        SoundPlayer.play(resolveFromCurrentPath(SOUND_EXECUTED));
                        this.executionController.execute(name.get());
                        return;
                    }
                }

                SoundPlayer.play(resolveFromCurrentPath(SOUND_FAILED));
            } catch (Exception e) {
                e.printStackTrace(); // fixme;
            }
        };
    }

    private void registerFunctions() {
        this.executionController = new ExecutionController();

        this.executionController.register("balkon_aan", KlikAanKlikUitService.balkonAanAction);
        this.executionController.register("balkon_uit", KlikAanKlikUitService.balkonUitAction);
        this.executionController.register("taart_aan", KlikAanKlikUitService.taartAanAction);
        this.executionController.register("taart_uit", KlikAanKlikUitService.taartUitAction);
        this.executionController.register("livingcolors_aan", KlikAanKlikUitService.livingColorsAanAction);
        this.executionController.register("livingcolors_uit", KlikAanKlikUitService.livingColorsUitAction);
        this.executionController.register("kastlampen_aan", KlikAanKlikUitService.kastlampAanAction);
        this.executionController.register("kastlampen_uit", KlikAanKlikUitService.kastlampUitAction);
        this.executionController.register("alles_aan", KlikAanKlikUitService.allesAanAction);
        this.executionController.register("alles_uit", KlikAanKlikUitService.allesUitAction);
    }

    private Path resolveFromCurrentPath(String file) throws URISyntaxException {
        return Paths.get(this.basePath.toString(), file);
    }

}
