package com.github.alex3305.voicecontrol;

import com.github.alex3305.voicecontrol.controllers.VoiceCommandController;

import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class VoiceControlApplication {

    public static void main(String[] args) throws URISyntaxException {
        Path base = Paths.get(VoiceCommandController.class.getProtectionDomain().getCodeSource().getLocation().toURI())
                .getParent();

        new VoiceCommandController(base).run();
    }

}
