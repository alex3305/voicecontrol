package com.github.alex3305.voicecontrol.services;

import com.github.alex3305.voicecontrol.entities.KeywordAction;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public interface KlikAanKlikUitService {

    String API_KEY = "bfa4bfb4b64ef8a1be3e5ad67f35e2e2e228b8ea04a51e7091c38cb6b3b49936d0a0e49df219c97a2de5219c8c19a9138fdd14cdcfd8cc8700b96651203be10b";

    String REQUEST_URL = "http://storagebox:3305/toggle/{apikey}/{lamp}/{state}";

    KeywordAction balkonAanAction = () -> doRequest(200, true);
    KeywordAction balkonUitAction = () -> doRequest(200, false);

    KeywordAction taartAanAction = () -> doRequest(201, true);
    KeywordAction taartUitAction = () -> doRequest(201, false);

    KeywordAction livingColorsAanAction = () -> doRequest(202, true);
    KeywordAction livingColorsUitAction = () -> doRequest(202, false);

    KeywordAction kastlampAanAction = () -> {
        taartAanAction.trigger();
        livingColorsAanAction.trigger();
    };
    KeywordAction kastlampUitAction = () -> {
        taartUitAction.trigger();
        livingColorsUitAction.trigger();
    };

    KeywordAction allesAanAction = () -> {
        balkonAanAction.trigger();
        kastlampAanAction.trigger();
    };
    KeywordAction allesUitAction = () -> {
        balkonUitAction.trigger();
        kastlampUitAction.trigger();
    };

    static void doRequest(int lamp, boolean state) throws IOException {
        URL url = new URL(REQUEST_URL
                .replace("{apikey}", API_KEY)
                .replace("{lamp}", String.valueOf(lamp))
                .replace("{state}", String.valueOf(state)));

        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("GET");

        BufferedReader rd = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        String line;
        while ((line = rd.readLine()) != null) {
            System.out.println(line);
        }
        rd.close();
    }

}
