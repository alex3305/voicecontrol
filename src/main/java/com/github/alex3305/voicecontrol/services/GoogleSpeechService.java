package com.github.alex3305.voicecontrol.services;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.Collections;
import java.util.List;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class GoogleSpeechService {

    public static final String API_KEY = "AIzaSyBOti4mM-6x9WDnZIjIeyEU21OpBXqWBgw";

    private static final String GOOGLE_URL = "https://www.google.com/speech-api/v2/recognize?output=json&lang={language}&key={apikey}&client=chromium&maxresults=2&pfilter=2";

    private static final String USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.110 Safari/537.36";

    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    public static List<String> transcribe(InputStream audio, Languages language) throws IOException {
        HttpURLConnection connection = createConnection(createUrl(language), audio);

        try (BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream(), Charset.forName("UTF-8")))) {
            String response;

            while ((response = br.readLine()) != null) {
                if (response.length() >= 20) {
                    return getResponse(response);
                }
            }
        }

        return Collections.emptyList();
    }

    private static URL createUrl(Languages language) throws MalformedURLException {
        return new URL(GOOGLE_URL.replace("{language}", language.getValue()).replace("{apikey}", API_KEY));
    }

    private static HttpURLConnection createConnection(URL url, InputStream audio) throws IOException {
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();

        connection.setRequestMethod("POST");
        connection.setRequestProperty("User-Agent", USER_AGENT);
        connection.setRequestProperty("Content-Type", "audio/x-flac; rate=44100;");
        connection.setDoOutput(true);

        copyOutputStreams(audio, connection);
        return connection;
    }

    private static void copyOutputStreams(InputStream audio, HttpURLConnection connection) throws IOException {
        OutputStream os = connection.getOutputStream();
        byte[] buffer = new byte[256];

        while ((audio.read(buffer, 0, 256)) != -1) {
            os.write(buffer, 0, 256);
        }

        audio.close();
        os.close();
    }

    private static List<String> getResponse(String json) throws IOException {
        JsonNode result = OBJECT_MAPPER.readTree(json).get("result").get(0);

        if (result.has("alternative")) {
            return StreamSupport.stream(Spliterators.spliteratorUnknownSize(result.get("alternative").iterator(), Spliterator.ORDERED), false)
                    .filter(t -> t.has("transcript"))
                    .map(t -> t.get("transcript").asText(""))
                    .filter(t -> !t.isEmpty())
                    .collect(Collectors.toList());
        }

        return Collections.emptyList();
    }

    public enum Languages {
        NL("nl_NL"),
        UK("en_UK"),
        US("en_US");

        final String language;

        Languages(String language) {
            this.language = language;
        }

        public String getValue() {
            return this.language;
        }
    }

}
