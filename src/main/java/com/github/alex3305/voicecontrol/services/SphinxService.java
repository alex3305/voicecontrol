package com.github.alex3305.voicecontrol.services;

import edu.cmu.sphinx.api.Configuration;
import edu.cmu.sphinx.api.LiveSpeechRecognizer;
import edu.cmu.sphinx.api.Microphone;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.TargetDataLine;
import java.io.IOException;
import java.lang.reflect.Field;

public class SphinxService {

    public static final AudioFormat SPHINX_AUDIO_FORMAT = new AudioFormat(16000, 16, 1, true, false);

    public static TargetDataLine acquireSphinxTargetDataLine(LiveSpeechRecognizer recognizer)
            throws IllegalAccessException, NoSuchFieldException {
        Field f = recognizer.getClass().getDeclaredField("microphone");
        f.setAccessible(true);
        Microphone microphone = (Microphone) f.get(recognizer);

        f = microphone.getClass().getDeclaredField("line");
        f.setAccessible(true);
        return (TargetDataLine) f.get(microphone);
    }

    public Configuration setupSphinx(String keywordPath, String fileName) {
        Configuration configuration = new Configuration();

        configuration.setAcousticModelPath("resource:/org/voxforge/dutch/models/nl-nl");
        configuration.setDictionaryPath("resource:/org/voxforge/dutch/models/celex.dic");
        configuration.setGrammarPath("resource:" + keywordPath);
        configuration.setGrammarName(fileName);
        configuration.setUseGrammar(true);

        return configuration;
    }

    public LiveSpeechRecognizer createRecognizer(String keywordPath, String fileName) throws IOException {
        return new LiveSpeechRecognizer(this.setupSphinx(keywordPath, fileName));
    }

}
